<?php

namespace App\Controllers;

class Pages extends BaseController
{
    public function index()
    {
        $data = [
            'title' => 'Home | Web Programming',
            'test' => ['satu', 'dua', 'tiga']
        ];

        return view('pages/home', $data);
    }

    public function about()
    {
        $data = [
            'title' => 'About Me | Web Programming'
        ];

        return view('pages/about', $data);
    }

    public function contact()
    {
        $data = [
            'title' => 'Contact Me | Web Programming',
            'alamat' => [
                [
                    'type' => 'rumah',
                    'alamat' => 'Lot 309A, Kampung Kelanang',
                    'poskod' => '42700',
                    'bandar' => 'Banting',
                    'negeri' => 'Selangor'
                ],
                [
                    'type' => 'pejabat',
                    'alamat' => 'Tingkat 1, MCMC Tower 1',
                    'poskod' => '63000',
                    'bandar' => 'Cyberjaya',
                    'negeri' => 'Selangor'
                ]
            ]
        ];

        return view('pages/contact', $data);
    }
}
